# React sportball

## Getting started
Install all dependencies listed in package.json using npm.
```
    npm install --save-dev {dependency}
```

### Bundle js code and run webpack-dev-server
```
    npm run start
``` 

### Author
* **Sotnichenko Roman** (Telegram: @RomanSo)

### License
This project is licensed under the MIT License.


